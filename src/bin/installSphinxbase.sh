#!/bin/sh

mkdir -p ../install
cd ../install

echo "Downloading sphinxbase..."
wget http://sourceforge.net/projects/cmusphinx/files/sphinxbase/5prealpha/sphinxbase-5prealpha.tar.gz
tar -xvf sphinxbase-5prealpha.tar.gz
rm sphinxbase-5prealpha.tar.gz
cd sphinxbase-5prealpha

# Remove sphinxbase.c -- needs to be generated on the build for Edison
rm python/sphinxbase.c

echo "Installing sphinxbase..."
./configure
make
make install

#echo "Installing Python bindings..."
#cd python
#python setup.py install

echo "Sphinxbase installed."
