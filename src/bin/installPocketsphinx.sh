#!/bin/sh

mkdir -p ../install
cd ../install

echo "Downloading pocketsphinx..."
wget http://sourceforge.net/projects/cmusphinx/files/pocketsphinx/5prealpha/pocketsphinx-5prealpha.tar.gz
tar -xvf pocketsphinx-5prealpha.tar.gz
rm pocketsphinx-5prealpha.tar.gz
cd pocketsphinx-5prealpha

# Remove pocketsphinx.c -- needs to be generated on the build for Edison
rm python/pocketsphinx.c

echo "Installing pocketsphinx..."
./configure
make
make install

#echo "Installing Python bindings..."
#cd python
#python setup.py install

echo "Pocketsphinx installed."
