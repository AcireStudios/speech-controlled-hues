#!/bin/sh

mkdir -p ../install
cd ../install

echo "Installing portaudio..."
wget -c http://www.portaudio.com/archives/pa_stable_v19_20140130.tgz
tar zxf pa_stable_v19_20140130.tgz
mv portaudio portaudio-v19
cd portaudio-v19
./configure
make
cd ..

echo "Building..."
export CFLAGS="-I `pwd`/portaudio-v19/include/ -L `pwd`/portaudio-v19/lib/.libs/"
python setup.py build --static-link
python setup.py install

echo "PortAudio installed."
