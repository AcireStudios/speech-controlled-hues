#include <pocketsphinx.h>
#include <sphinxbase.h>
#include <portaudio.h>

#include <iostream>
#include <unistd.h>

#include "speechDecoder.h"
#include "hueController.h"

using namespace std;

/* #define SAMPLE_RATE
 #define FRAMES_PER_BUFFER (512)
 #define NUM_SECONDS     (5)
 #define NUM_CHANNELS    (2)
 /* #define DITHER_FLAG     (paDitherOff) */
#define DITHER_FLAG     (0)

/* Select sample format. */
#if 1
#define PA_SAMPLE_TYPE  paFloat32
typedef float SAMPLE;
#define SAMPLE_SILENCE  (0.0f)
#elif 1
#define PA_SAMPLE_TYPE  paInt16
typedef short SAMPLE;
#define SAMPLE_SILENCE  (0)
#elif 0
#define PA_SAMPLE_TYPE  paInt8
typedef char SAMPLE;
#define SAMPLE_SILENCE  (0)
#else
#define PA_SAMPLE_TYPE  paUInt8
typedef unsigned char SAMPLE;
#define SAMPLE_SILENCE  (128)
#endif

typedef struct {
	int frameIndex; /* Index into sample array. */
	int maxFrameIndex;
	SAMPLE *recordedSamples;
} paTestData;

static void signalHues(HueController lights, string words) {
	switch (words) {
	case "lights":
		lights.setAllOn();
		break;
	case "all":
		lights.selectAll();
		break;
	case "one":
		lights.selectLight(1);
		break;
	case "two":
		lights.selectLight(2);
		break;
	case "three":
		lights.selectLight(3);
		break;
	case "red":
		lights.setSelectedColorTo(0, 200, 220);
		break;
	case "green":
		lights.setSelectedColorTo(25500, 200, 220);
		break;
	case "blue":
		lights.setSelectedColorTo(46920, 200, 220);
		break;
	default:
		lights.setAllOn();
	}
}

int main() {
	SpeechDecoder decoder;
	HueController hues;

	string voiceCommandStr;

	if (decoder.init() > 0) {
		cerr << "error on init" << endl;
		return 1;
	}

	string fileLoc = "recorded.raw";

	PaStreamParameters paramsIn, paramsOut;
	PaStream* stream;
	PaError err = paNoError;
	paTestData data;
	int i;
	int totalFrames;
	int numSamples;
	int numBytes;
	SAMPLE max, val;
	double average;

	while (true) {
		data.maxFrameIndex = totalFrames = NUM_SECONDS * SAMPLE_RATE;
		data.frameIndex = 0;
		numSamples = totalFrames * NUM_CHANNELS;
		numBytes = numSamples * sizeof(SAMPLE);
		data.recordedSamples = (SAMPLE *) malloc(numBytes);
		if (data.recordedSamples == NULL) {
			cout << "Could not allocate space for recording" << endl;
			return 0;
		}
		for (i = 0; i < numSamples; i++)
			data.recordedSamples[i] = 0;

		err = Pa_Initialize();
		if (err != paNoError)
			return 0;

		paramsIn.device = Pa_GetDefaultInputDevice();
		if (paramsIn.device == paNoDevice) {
			cout << "Cannot find audio device" << endl;
			return 0;
		}
		paramsIn.channelCount = 2;
		paramsIn.sampleFormat = PA_SAMPLE_TYPE;
		paramsIn.suggestedLatency =
				Pa_GetDeviceInfo(paramsIn.device)->defaultLowInputLatency;
		paramsIn.hostApiSpecificStreamInfo = NULL;

		/* Record audio*/
		err = Pa_OpenStream(&stream, &paramsIn,
		NULL, SAMPLE_RATE, FRAMES_PER_BUFFER, paClipOff, recordCallback, &data);
		if (err != paNoError)
			return 0;

		err = Pa_StartStream(stream);
		if (err != paNoError)
			return 0;
		cout << "Listening... " << endl;

		while ((err = Pa_IsStreamActive(stream)) == 1) {
			Pa_Sleep(1000);
		}
		if (err < 0)
			return 0;

		err = Pa_CloseStream(stream);
		if (err != paNoError)
			return 0;

		max = 0;
		average = 0.0;
		for (i = 0; i < numSamples; i++) {
			val = data.recordedSamples[i];
			if (val < 0)
				val = -val;
			if (val > max) {
				max = val;
			}
			average += val;
		}

		average = average / (double) numSamples;

		/* Write recorded data to fileLoc */
		FILE *fid;
		fid = fopen(fileLoc.c_str(), "wb");
		if (fid == NULL) {
			cout << "Cannot open file" << endl;
		} else {
			fwrite(data.recordedSamples, NUM_CHANNELS * sizeof(SAMPLE),
					totalFrames, fid);
			fclose(fid);
			cout << "Recorded data written to 'recorded.raw'" << endl;
		}

		voiceCommandStr = decoder.rawToString(fileLoc.c_str());
		signalHues(hues, voiceCommandStr);

	} //end while

}

