#include "speechDecoder.h"
//a handler that will loop and listen for commands
//passing the recognized words to the logic node for processing

SpeechDecoder::SpeechDecoder()
{
	//language model definition
	lmDir = "/models/language/1534.lm";
	dicDir = "/models/language/1534.dic";
}

SpeechDecoder::~SpeechDecoder()
{
	fclose(fh);
    ps_free(ps);
}

//pocketsphinx cmd_ln.c interpreter
int SpeechDecoder::init()
{
	//create a config object
	config = cmd_ln_init(NULL, ps_args(), TRUE,
			     "-lm", lmDir.c_str(),
			     "-dict", dicDir.c_str(),
			     NULL);
	if (config == NULL)
		return 1;
		
	//initializing the decoder
	ps = ps_init(config);
	if (ps == NULL)
		return 1;
	
	return 0;
}

int SpeechDecoder::setLMdir(std::string _lmDir)
{
	
	lmDir = _lmDir;
	if(lmDir.c_str()==NULL)
		return -1;
		
	updateModel();//update if config already exists
	return 1;
}

int SpeechDecoder::setDICdir(std::string _dicDir)
{
	
	dicDir = _dicDir;
	if(dicDir.c_str() ==NULL)
		return -1;
	
	updateModel(); //update if config object already exists
	return 1;
}

int SpeechDecoder::updateModel()
{
	if(config == NULL)
		return 1;
	
	ps_free(ps); //free the decoder
	
	init(); //loads the decoder with the new language model.
	
}

std::string SpeechDecoder::rawToString(const char * pathToraw)
{
	std::string er = "error";
	//open the file stream for reading in binary mode
	fh = fopen(pathToraw, "rb");
	if (fh == NULL) {
		perror("Failed to open raw file");
		return er;
	}
	
	//decode the binary audio files and return the number of samples recovered
	rv = ps_decode_raw(ps, fh, "myUttId", -1);
	if (rv < 0)
		return er;
		
	//get the hypothesis on the output 	
	hyp = ps_get_hyp(ps, &score, &uttid);
	
	if (hyp == NULL)
		{
			return WNF;
		}
	
	return hyp;	
}


