#ifndef _HUECONTROLLER_H_
#define _HUECONTROLLER_H_

#include <string>
#include <iostream>
#include <sstream>

#include "../../helpers/utils.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/StreamCopier.h"
#include "Poco/Path.h"
#include "Poco/URI.h"
#include "Poco/Exception.h"

using namespace std;

using namespace Poco::Net;
using namespace Poco;
using Poco::Exception;
using Poco::Net::HTTPClientSession;



class HueController{

public:

	bool allSelected;
	int currentSelected;

	HueController();

	void setup(string bridgeIP, string userName);

	void setLightStates(	int lightID,
					   bool state,
					   int h = 0,
					   int b = 0,
					   int s = 0
	);

	void setAllStates(	bool state,
						int h = 0,
						int b = 0,
						int s = 0
	);

	void selectLight(int lightID);

	void selectAll();

	void setStateOn(int lightID);

	void setStateOff(int lightID);

	void setAllOn();

	void setLightColor(int lightID, int h, int s, int b);

	void setAllColor(int h, int s, int b);

	void setSelectedColorTo(int h, int s, int b);


private:
	string buildStatesJSON(	bool state,
						   int h = 0,
						   int b = 0,
						   int s = 0
		);
	string getSelectionJSON();

	string getSelectionJSON();

	string getStateJSON(bool state);

	bool sendCommandToLight(int lightID, string json);

	bool sendCommandToGroup(int groupID, string json);

	bool sendCommand(Poco::URI uri, string json);

	string bridge;
	string apiUser;

};


#endif
