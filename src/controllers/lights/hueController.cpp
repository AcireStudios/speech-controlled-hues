#include "hueController.h"

HueController::HueController(){
	bridge = "";
	apiUser = "";
	allSelected = false;
	currentSelected = -1;
}

void HueController::setup(string bridgeIP, string userName){

	bridge = bridgeIP;
	apiUser = userName;
}
//Main Commands
void HueController::selectLight(int lightID){

	if(sendCommandToLight(lightID, getSelectionJSON())){
		allSelected = false;
		currentSelected = lightID;
	}
}

void HueController::selectAll(){

	if(sendCommandToGroup(0, getSelectionJSON())){
		currentSelected = -1;
		allSelected = true;
	}
}

void HueController::setStateOn(int lightID){
	sendCommandToLight(lightID, getStateJSON(true));
}

void HueController::setStateOff(int lightID){
	sendCommandToLight(lightID, getStateJSON(false));
}

void HueController::setAllOn(){
	sendCommandToGroup(0, getStateJSON(false));
}

void HueController::setLightColor(int lightID, int h, int s, int b){
	setLightStates(lightID, true, h, s, b );
}

void HueController::setAllColor(int h, int s, int b){
	setAllStates(true, h, s, b );
}

void HueController::setSelectedColorTo(int h, int s, int b){
	if(currentSelected > 0 && currentSelected <= 3){
		setLightColor(currentSelected, h, s, b);
	}else{//select all and apply color
		allSelected = true;
		setAllColor(h, s, b);
	}
}

//Helper functions
string HueController::buildStatesJSON(bool state, int h, int b, int s){

	//build json data
	string json = "{ \"on\":" + string( state ? "true" : "false");

	if (b >= 0){
		json += string(" , ") + "\"bri\":" + utils_ToString(b);
	}

	if (s >= 0){
		json += string(" , ") + "\"sat\":" + utils_ToString(s);
	}

	if (h >= 0){
		json += string(" , ") + "\"hue\":" + utils_ToString(h);
	}

	json += " }";


	return json;

}

string HueController::getSelectionJSON(){
	//build json data
	return "{ \"on\":true, \"alert\":\"select\" }";
}

string HueController::getStateJSON(bool state){
	//build json data
	return "{ \"on\":"+string( state ? "true" : "false")+ "}";
}

void HueController::setLightStates(int lightID, bool state, int h, int b, int s){
	string json = buildStatesJSON(state, h, b, s);
	sendCommandToLight(lightID, json);
}

void HueController::setAllStates( bool state, int h, int b, int s){
	string json = buildStatesJSON(state, h, b, s);
	sendCommandToGroup(0, json);
}

//Send REST API calls
bool HueController::sendCommandToLight(int lightID, string json){
	if (bridge.length() == 0 || apiUser.length() == 0){
			cout << "HueController::setLightStates(); Can't set Light States! You need to setup first!" << endl;
			return false;
		}

	Poco::URI uri = Poco::URI( "http://" + bridge + "/api/" + apiUser +"/lights/" + utils_ToString(lightID) + "/state" );

	return sendCommand(uri, json);
}

bool HueController::sendCommandToGroup(int groupID, string json){
	if (bridge.length() == 0 || apiUser.length() == 0){
			cout << "HueController::setAllStates(); Can't set Group States! You need to setup first!" << endl;
			return false;
		}

	Poco::URI uri = Poco::URI( "http://" + bridge + "/api/" + apiUser +"/groups/" + utils_ToString(groupID) + "/action" );

	return sendCommand(uri, json);
}

bool HueController::sendCommand(Poco::URI uri, string json){

	if(uri == NULL){
		return false;
	}

	float timeOut = 1.0; //seconds
	string path(uri.getPathAndQuery());
	if (path.empty()) path = "/";
	string host = uri.getHost();


	try{

		HTTPClientSession session( host, uri.getPort() );
		HTTPRequest req(HTTPRequest::HTTP_PUT, path, HTTPMessage::HTTP_1_1);

		session.setTimeout( Poco::Timespan(timeOut,0) );
		req.set( "User-Agent", "HueController");
		req.setContentLength( json.length() );

		ostream& os = session.sendRequest(req);
		istringstream is( json );
		Poco::StreamCopier::copyStream(is, os);
		req.setContentLength( json.length() );

		Poco::Net::HTTPResponse res;
		istream& rs = session.receiveResponse(res);
		string responseBody = "";
		StreamCopier::copyToString(rs, responseBody);	//copy the data...
		cout << "HueController >> Response : " << responseBody << endl << endl;
		return true;
	}catch(Exception& exc){
		//ofLog( OF_LOG_ERROR, "HueController::sendCommand(%s) to %s >> Exception: %s\n", json.c_str(), uri.toString().c_str(), exc.displayText().c_str() );
		return false;
	}

}




