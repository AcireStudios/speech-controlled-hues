#ifndef HELPERS_UTILS_H_
#define HELPERS_UTILS_H_

#include <string>
#include <iostream>
#include <sstream>

using namespace std;

template <class T>
	string utils_ToString(const T& value){
	ostringstream out;
	out << value;
	return out.str();
}


#endif /* HELPERS_UTILS_H_ */
